#!/usr/bin/env bash

BASE_DIR="$(pwd)"
SOURCEDIR="${BASE_DIR}/work"
token=$TOKEN

curl https://rclone.org/install.sh | sudo bash
rm -rf "${SOURCEDIR}"
df -h
mkdir -p "${SOURCEDIR}"
cd "${SOURCEDIR}"
export ALLOW_MISSING_DEPENDENCIES=true
repo init --depth=1 -u https://cyberknight777:$token@github.com/Not-So-Pixel/manifest.git -b new
sed -i "s|ssh://git@github.com/Not-So-Pixel|https://cyberknight777:$token@github.com/Not-So-Pixel|g" .repo/manifests/snippets/pixel.xml
sed -i "s|ssh://git@github.com/dsashwin|https://github.com/dsashwin|" .repo/manifests/snippets/pixel.xml
repo sync -c -j4 --force-sync --no-clone-bundle --no-tags

rm -rf hardware/qcom-caf/sm8150/audio
rm -rf hardware/qcom-caf/sm8150/display
rm -rf hardware/qcom-caf/sm8150/media
rm -rf hardware/qcom-caf/wlan
rm -rf hardware/qcom/wlan
rm -rf packages/resources/devicesettings
rm -rf system/memory/lmkd
rm -rf system/core
rm -rf build/make
rm -rf external/erofs-utils

git clone --depth=1 https://github.com/pixelos-devices/device_oneplus_hotdogg.git device/oneplus/hotdogg
git clone --depth=1 https://github.com/pixelos-devices/device_oneplus_sm8150-common.git device/oneplus/sm8150-common
git clone --depth=1 https://github.com/pixelos-devices/vendor_oneplus_hotdogg.git vendor/oneplus/hotdogg
git clone --depth=1 https://github.com/pixelos-devices/vendor_oneplus_sm8150-common.git vendor/oneplus/sm8150-common
git clone --depth=1 https://github.com/pixelos-devices/kernel_oneplus_sm8150.git kernel/oneplus/sm8150
git clone --depth=1 https://github.com/cyberknight777/gcc-arm64-old prebuilts/gcc/linux-x86/aarch64/aarch64-elf
git clone --depth=1 https://github.com/cyberknight777/gcc-arm-old prebuilts/gcc/linux-x86/arm/arm-eabi
git clone --depth=1 https://github.com/PixelOS-Devices/hardware_oneplus hardware/oneplus
git clone --depth=1 https://github.com/YAAP/hardware_qcom-caf_sm8150_audio hardware/qcom-caf/sm8150/audio
git clone --depth=1 https://github.com/YAAP/hardware_qcom-caf_sm8150_display hardware/qcom-caf/sm8150/display
git clone --depth=1 https://github.com/YAAP/hardware_qcom-caf_sm8150_media hardware/qcom-caf/sm8150/media
git clone --depth=1 https://github.com/YAAP/hardware_qcom-caf_wlan hardware/qcom-caf/wlan
git clone --depth=1 https://github.com/YAAP/hardware_qcom_wlan hardware/qcom/wlan
git clone --depth=1 https://github.com/LineageOS/android_packages_resources_devicesettings packages/resources/devicesettings
git clone --depth=1 https://github.com/cyberknight777/vendor_csettings vendor/csettings
git clone --depth=1 https://github.com/PixelOS-Devices/system_memory_lmkd system/memory/lmkd
git clone --depth=1 https://github.com/CannedShroud/android_packages_apps_KProfiles packages/apps/KProfiles
git clone --depth=1 https://github.com/PixelOS-Pixelish/system_core system/core
git clone --depth=1 https://github.com/cyberknight777/build build/make
git clone --depth=1 https://github.com/YAAP/external_erofs-utils external/erofs-utils

cd kernel/oneplus/sm8150 || exit 1
./scripts/config --file arch/arm64/configs/dragonheart_defconfig --set-str CONFIG_LOCALVERSION "-DragonHeart-PixelOS"
cd ../../../ || exit 1

. build/envsetup.sh
lunch aosp_hotdogg-user
mka clean
lunch aosp_hotdogg-user
mka bacon

cd out/target/product/hotdogg
wget https://gist.githubusercontent.com/noobyysauraj/8a0a66cc3fd3f5a513a4eee3f5625b38/raw/a079327aa326cf916df6704d28778f81566a0b82/rclone.conf
mkdir $HOME/.config/rclone/
mv rclone.conf $HOME/.config/rclone/
rclone -P copy PixelOS*.zip oned:/MY_BOMT_STUFFS/cyberbhai/PixelOS

exit 0
